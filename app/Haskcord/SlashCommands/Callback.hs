module Haskcord.SlashCommands.Callback where

import Universum

import Discord (DiscordHandler)
import Discord.Interactions (OptionValue, OptionDataValue(..), Interaction)

import Haskcord.SlashCommands.Internal.Types (HA(..), HList(..))
import Haskcord.SlashCommands.Internal.OptionValueParsers (FromOptionDataValue(..))

type CommandIO = ReaderT Interaction DiscordHandler

type family HaskcordCallback a where
  HaskcordCallback (HList (HA x Identity ': xs)) = x       -> HaskcordCallback (HList xs)
  HaskcordCallback (HList (HA x Maybe ': xs))    = Maybe x -> HaskcordCallback (HList xs)
  
  HaskcordCallback (HList '[]) = CommandIO ()

class HaskcordOptions args where
  toValues :: args -> [OptionValue]
  mkCallback
    :: HaskcordCallback args
    -> Maybe args
    -> [Text]
    -> [OptionDataValue]
    -> Interaction
    -> DiscordHandler ()

instance HaskcordOptions (HList '[]) where
  toValues _ = []

  mkCallback fn _ _ opts interaction =
    case opts of
      [] -> runReaderT fn interaction 
      _ -> pure ()

instance (FromOptionDataValue x, HaskcordOptions (HList xs)) => HaskcordOptions (HList (HA x Identity ': xs)) where
  toValues (ha@HA{} :-: xs) = ha.val : toValues xs

  mkCallback fn args names opts interaction =
    case (opts, names, args) of
      (opt : os, n : ns, Just (_ :-: as)) -> do
        opt' <- fromOptionDataValue opt
        case (opt', opt.optionDataValueName) of
          (Just o, m) | n == m -> do
            putTextLn $ "positive case for Identity unwrapping with value: " <> n
            mkCallback @(HList xs) (fn o) (Just as) ns os interaction
          (Just _, _) -> putTextLn "misaligned name in Identity"
          _ -> putTextLn "misaligned type in Identity"
      _ -> putTextLn "misaligned opts # in Identity"

instance (FromOptionDataValue x, HaskcordOptions (HList xs)) => HaskcordOptions (HList (HA x Maybe ': xs)) where
  toValues (ha@HA{} :-: xs) = ha.val : toValues xs

  mkCallback fn args names opts interaction =
    case (opts, names, args) of
      (opt : os, n : ns, Just (_ :-: as)) -> do
        opt' <- fromOptionDataValue opt
        case opt' of
          (Just o) | n == opt.optionDataValueName -> do
            putTextLn $ "positive case for Maybe unwrapping with value: " <> n
            mkCallback @(HList xs) (fn (Just o)) (Just as) ns os interaction
          Just _ -> putTextLn "misaligned name in Maybe"
          Nothing -> do
            putTextLn "positive case for Maybe unwrapping without value: "
            mkCallback @(HList xs) (fn Nothing) (Just as) ns opts interaction
      (_, _, Just (_ :-: as)) -> mkCallback @(HList xs) (fn Nothing) (Just as) names opts interaction
      _ -> putTextLn "misaligned hlist type"

