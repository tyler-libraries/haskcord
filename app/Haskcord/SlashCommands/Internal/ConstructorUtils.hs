module Haskcord.SlashCommands.Internal.ConstructorUtils where

import Universum
import PyF

import qualified Data.Char as C
import qualified Data.Text as T

import Haskcord.SlashCommands.Types (CommandCallbacks(..))

type family MkOptionOutput (b :: Bool) where
  MkOptionOutput 'True = Identity
  MkOptionOutput 'False = Maybe

ifValidNameAndDescription :: Text -> Text -> a -> Either Text a
ifValidNameAndDescription name description fn
    | null name || length name > 32 || not (all validCharacter name) =
      Left [fmt|{name} is an invalid command name|]
    | null description || length description > 100 =
      Left [fmt|{description} is an invalid command description for {name}|]
    | otherwise = Right fn
    where validCharacter c = c == '-' || c == '_' || C.isLower c || C.isNumber c

display :: CommandCallbacks -> Text
display (CommandCallbackNode n cs) =
  [fmt|CommandCallbackNode {n}[{T.intercalate "," (map display cs)}]|]
display (CommandCallbackLeaf n _) = 
  [fmt|Leaf {n}|]

emptyFn :: Monad m => (a -> m ())
emptyFn = const $ pure ()
