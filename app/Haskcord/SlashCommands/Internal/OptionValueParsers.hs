module Haskcord.SlashCommands.Internal.OptionValueParsers where

import Universum

import Discord.Interactions (OptionDataValue (..), Choice(..), OptionValue (..))

import Haskcord.SlashCommands.Internal.Types
import qualified Haskcord.User as User
import Discord.Types
import Discord
import Haskcord.User
import Discord.Requests

class FromOptionDataValue a where
  fromOptionDataValue :: OptionDataValue -> DiscordHandler (Maybe a)

instance FromOptionDataValue  Bool where
  fromOptionDataValue OptionDataValueBoolean {optionDataValueBoolean=boolean} =
    pure $ Just boolean
  fromOptionDataValue _ = pure Nothing

instance FromOptionDataValue Text where
  fromOptionDataValue OptionDataValueString {optionDataValueString = Right string} =
    pure $ Just string
  fromOptionDataValue _ = pure Nothing

instance FromOptionDataValue Integer where
  fromOptionDataValue OptionDataValueInteger {optionDataValueInteger = Right int} = 
    pure $ Just int
  fromOptionDataValue _ = pure Nothing

instance FromOptionDataValue VoiceChannel where
  fromOptionDataValue OptionDataValueChannel {optionDataValueChannel = channelId} = do
    cache <- readCache
    let 
      guildCache  = cacheGuilds cache
      voiceStates = guildCache & elems & catMaybes & mapMaybe snd >>= guildCreateVoiceStates
        & filter (\v -> v.voiceStateChannelId == channelId)

    users <- voiceStates & traverse (\vs -> restCall (GetUser vs.voiceStateUserId)) <&> sequence
    
    channel <- restCall $ GetChannel channelId
    pure $ case (channel, users) of
      (Right c, Right us) -> Just $ VoiceChannel
        { id       = channelId
        , voiceChannelName = c.name
        , guildId  = c.channelGuild
        , users    = us
        , position = c.channelPosition
        , nsfw     = c.channelNSFW
        }
      _ -> Nothing
  fromOptionDataValue _ = pure Nothing


class ToOptionValue a f cs acs | a -> cs, a -> acs where
  toOptionValue :: Text -> Text -> Bool -> Maybe cs -> Maybe acs -> HA a f

instance ToOptionValue Text f [Text] [Choice Text] where
  toOptionValue name desc req cs acs = 
    HA (OptionValueString name Nothing desc Nothing req (parseChoices cs acs Universum.id) Nothing Nothing) req

instance ToOptionValue Integer f [Integer] [Choice Integer] where
  toOptionValue name desc req cs acs = 
    HA (OptionValueInteger name Nothing desc Nothing req (parseChoices cs acs show) Nothing Nothing) req

instance ToOptionValue Bool f () () where
  toOptionValue name desc req _  _ = HA (OptionValueBoolean name Nothing desc Nothing req) req

instance ToOptionValue VoiceChannel f () () where
  toOptionValue name desc req _  _ = HA (OptionValueChannel name Nothing desc Nothing req Nothing) req

parseChoices 
  :: Functor f 
  => Maybe (f t)
  -> Maybe (f (Choice t))
  -> (t -> Text)
  -> Either Bool (f (Choice t))
parseChoices cs acs toTextFn = parseSimpleChoices <> parseAdvancedChoices 
  where
    parseSimpleChoices = case cs of
      Just cs' -> Right $ map (\c -> Choice (toTextFn c) Nothing c) cs'
      Nothing -> Left False

    parseAdvancedChoices = case acs of
      Just c -> Right c
      Nothing -> Left False

