module Haskcord.SlashCommands.Internal.Types where

import Universum
import Discord.Interactions (OptionValue)

data HList xs where
  HNil :: HList '[]
  (:-:) :: HA x f -> HList xs -> HList (HA x f ': xs)
infixr 5 :-:

data HA a (f :: Type -> Type) = HA
  { val :: OptionValue
  , required :: Bool
  }
