module Haskcord.SlashCommands.Internal.TupleToHList where

import Haskcord.SlashCommands.Internal.Types

class TupleToHList a b | a -> b where
  tupleToHList :: a -> b

instance TupleToHList () (HList '[]) where
  tupleToHList _ = HNil

instance TupleToHList (HA a fa) (HList (HA a fa ': '[])) where
  tupleToHList haa = haa :-: HNil

instance TupleToHList (HA a fa, HA b fb) (HList (HA a fa ': HA b fb ': '[])) where
  tupleToHList (haa, hab) = haa :-: hab :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc) (HList (HA a fa ': HA b fb ': HA c fc ': '[])) where
  tupleToHList (haa, hab, hac) = haa :-: hab :-: hac :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': '[])) where
  tupleToHList (haa, hab, hac, had) = haa :-: hab :-: hac :-: had :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': '[])) where
  tupleToHList (haa, hab, hac, had, hae) = haa :-: hab :-: hac :-: had :-: hae :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi, HA j fj, HA k fk) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': HA j fj ': HA k fk ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai, haj, hak) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: haj :-: hak :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi, HA j fj, HA k fk, HA l fl) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': HA j fj ': HA k fk ': HA l fl ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai, haj, hak, hal) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: haj :-: hak :-: hal :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi, HA j fj, HA k fk, HA l fl, HA m fm) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': HA j fj ': HA k fk ': HA l fl ': HA m fm ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai, haj, hak, hal, ham) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: haj :-: hak :-: hal :-: ham :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi, HA j fj, HA k fk, HA l fl, HA m fm, HA n fn) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': HA j fj ': HA k fk ': HA l fl ': HA m fm ': HA n fn ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai, haj, hak, hal, ham, han) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: haj :-: hak :-: hal :-: ham :-: han :-: HNil

instance TupleToHList (HA a fa, HA b fb, HA c fc, HA d fd, HA e fe, HA f ff, HA g fg, HA h fh, HA i fi, HA j fj, HA k fk, HA l fl, HA m fm, HA n fn, HA o fo) (HList (HA a fa ': HA b fb ': HA c fc ': HA d fd ': HA e fe ': HA f ff ': HA g fg ': HA h fh ': HA i fi ': HA j fj ': HA k fk ': HA l fl ': HA m fm ': HA n fn ': HA o fo ': '[])) where
  tupleToHList (haa, hab, hac, had, hae, haf, hag, hah, hai, haj, hak, hal, ham, han, hao) = haa :-: hab :-: hac :-: had :-: hae :-: haf :-: hag :-: hah :-: hai :-: haj :-: hak :-: hal :-: ham :-: han :-: hao :-: HNil

