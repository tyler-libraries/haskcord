module Haskcord.SlashCommands.Types where
import Universum

import Discord (DiscordHandler)
import Discord.Types (GuildId, UserId, GuildMember, ChannelId)
import qualified Discord.Types as DT
import Discord.Interactions (CreateApplicationCommand, OptionDataValue, Interaction, OptionSubcommandOrGroup)
import Haskcord.SlashCommands.Internal.Types (HA)

data CommandCallbacks
  = CommandCallbackNode Text [CommandCallbacks]
  | CommandCallbackLeaf Text
    ([OptionDataValue] -> Interaction -> DiscordHandler ())

newtype Command = Command 
  ( Either Text CreateApplicationCommand
  , [GuildId]
  , CommandCallbacks
  )

data Subcommand = Subcommand
  { value    :: OptionSubcommandOrGroup
  , callback :: CommandCallbacks
  }

data SubcommandGroup = SubcommandGroup
  { value     :: OptionSubcommandOrGroup
  , callbacks :: CommandCallbacks
  }

type RequiredCommandOption a = HA a Identity
type OptionalCommandOption a = HA a Maybe
