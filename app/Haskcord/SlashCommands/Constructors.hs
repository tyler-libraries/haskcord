module Haskcord.SlashCommands.Constructors where

import Universum
import Named
import Discord.Interactions 

import Haskcord.Types
import Haskcord.SlashCommands.Types
import Haskcord.SlashCommands.Callback

import Haskcord.SlashCommands.Internal.Types
import Haskcord.SlashCommands.Internal.ConstructorUtils
import Haskcord.SlashCommands.Internal.OptionValueParsers
import Haskcord.SlashCommands.Internal.TupleToHList

mkCommandGroup
  :: "name"                     :! Text
  -> "description"              :! Text
  -> "subcommands"              :? [Subcommand]
  -> "subcommandGroups"         :? [SubcommandGroup]
  -> "guildIds"                 :? [Text]
  -> "localizedName"            :? LocalizedText
  -> "localizedDescription"     :? LocalizedText
  -> "defaultMemberPermissions" :? Text
  -> "dmPermission"             :? Bool
  -> Command
mkCommandGroup
  (Arg name)
  (Arg description)
  (argDef #subcommands [] -> subcommands)
  (argDef #subcommandGroups [] -> subcommandGroups)
  (argDef #guildIds [] -> guildIds)
  (ArgF localizedName)
  (ArgF localizedDescription)
  (ArgF defaultMemberPermissions)
  (ArgF dmPermission)
  =
  let
    appCmd = ifValidNameAndDescription name description $
      CreateApplicationCommandChatInput
        { createName                     = name
        , createLocalizedName            = localizedName
        , createDescription              = description
        , createLocalizedDescription     = localizedDescription
        , createOptions                  = Just $ OptionsSubcommands $
            map (\s -> s.value) subcommands <> map (\s -> s.value) subcommandGroups
        , createDefaultMemberPermissions = defaultMemberPermissions
        , createDMPermission             = dmPermission
        }
    in Command 
      ( appCmd
      , mapMaybe readMaybe guildIds
      , CommandCallbackNode name (map (\s -> s.callback) subcommands <> map (\s -> s.callbacks) subcommandGroups)
      )

mkCommand
  :: forall args hargs. (TupleToHList args hargs, HaskcordOptions hargs)
  => "name"                     :! Text
  -> "description"              :! Text
  -> "options"                  :! args
  -> "callback"                 :! HaskcordCallback hargs
  -> "guildIds"                 :? [Text]
  -> "localizedName"            :? LocalizedText
  -> "localizedDescription"     :? LocalizedText
  -> "defaultMemberPermissions" :? Text
  -> "dmPermission"             :? Bool
  -> Command
mkCommand
  (Arg name)
  (Arg description)
  (Arg options)
  (Arg callback)
  (argDef #guildIds [] -> guildIds)
  (ArgF localizedName)
  (ArgF localizedDescription)
  (ArgF defaultMemberPermissions)
  (ArgF dmPermission)
  =
  let
    res = tupleToHList @args @hargs options
    appCmd = ifValidNameAndDescription name description $
      CreateApplicationCommandChatInput
        { createName                     = name
        , createLocalizedName            = localizedName
        , createDescription              = description
        , createLocalizedDescription     = localizedDescription
        , createOptions                  = Just . OptionsValues $ toValues res
        , createDefaultMemberPermissions = defaultMemberPermissions
        , createDMPermission             = dmPermission
        }
    in Command
      ( appCmd
      , mapMaybe readMaybe guildIds
      , CommandCallbackLeaf name . mkCallback @hargs callback (Just res) . maybe [] (map optionValueName) . Just $ toValues res
      )

mkSubcommandGroup 
  :: "name"                     :! Text
  -> "description"              :! Text
  -> "subcommands"              :! [Subcommand]
  -> "localizedName"            :? LocalizedText
  -> "localizedDescription"     :? LocalizedText
  -> SubcommandGroup
mkSubcommandGroup
  (Arg name)
  (Arg description)
  (Arg subcommands)
  (ArgF localizedName)
  (ArgF localizedDescription)
  =
  let
    opts = catMaybes $ map (\s -> s.value) subcommands <&> \case
      OptionSubcommandGroup{}             -> Nothing
      OptionSubcommandOrGroupSubcommand o -> Just o
    subcommandGroup = OptionSubcommandGroup
      { optionSubcommandGroupName                 = name
      , optionSubcommandGroupLocalizedName        = localizedName
      , optionSubcommandGroupDescription          = description
      , optionSubcommandGroupLocalizedDescription = localizedDescription
      , optionSubcommandGroupOptions              = opts
      }
    callbacks = map (\s -> s.callback) subcommands
  in SubcommandGroup subcommandGroup (CommandCallbackNode name callbacks)

mkSubcommand
  :: forall args hargs. (TupleToHList args hargs, HaskcordOptions hargs)
  => "name"                 :! Text 
  -> "callback"             :! HaskcordCallback hargs
  -> "description"          :! Text
  -> "options"              :? args
  -> "localizedName"        :? LocalizedText
  -> "localizedDescription" :? LocalizedText
  -> Subcommand
mkSubcommand
  (Arg name)
  (Arg callback)
  (Arg description)
  (ArgF options)
  (ArgF localizedName)
  (ArgF localizedDescription)
  =
  let
    res  = options <&> tupleToHList @args @hargs
    opts = fromMaybe [] $ res <&> toValues
    subcommand = OptionSubcommandOrGroupSubcommand $ OptionSubcommand
      { optionSubcommandName                 = name
      , optionSubcommandLocalizedName        = localizedName
      , optionSubcommandDescription          = description
      , optionSubcommandLocalizedDescription = localizedDescription
      , optionSubcommandOptions              = opts
      }
  in Subcommand subcommand 
    . CommandCallbackLeaf name 
    . mkCallback @hargs callback res 
    . maybe [] (map optionValueName)
    $ res <&> toValues


mkOption
  :: (ToOptionValue a (MkOptionOutput b) cs acs)
  => "name"            :! Text
  -> "description"     :! Text
  -> "required"        :! TypeBool b
  -> "choices"         :? cs
  -> "advancedChoices" :? acs
  -> "autocomplete"    :? Bool
  -> HA a (MkOptionOutput b)
mkOption
  (Arg name)
  (Arg description)
  (Arg required)
  (ArgF choices)
  (ArgF advancedChoices)
  (ArgF autocomplete)
  = toOptionValue name description (case required of
    True'  -> True
    False' -> False) choices advancedChoices

mkChoice
  :: Show a 
  =>  "value"        :! a
  -> "displayName"   :? Text
  -> "localizedName" :? LocalizedText
  -> Choice a
mkChoice
  (Arg value)
  (ArgF displayName)
  (ArgF localizedName)
  = Choice name localizedName value
    where name = fromMaybe (show value) displayName
