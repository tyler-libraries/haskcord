module Haskcord.Types where

import Universum
import Discord (DiscordHandler)

data TypeBool (b :: Bool) where
  True'  :: TypeBool 'True
  False' :: TypeBool 'False

