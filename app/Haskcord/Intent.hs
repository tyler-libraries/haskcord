module Haskcord.Intent where

import Universum

data Gateway
  = Guilds 
  | Members 
  | Bans 
  | Emojis 
  | Integrations 
  | Webhooks 
  | Invites 
  | VoiceStates 
  | Presences 
  | MessageChanges 
  | MessageReactions 
  | MessageTyping 
  | DirectMessageChanges 
  | DirectMessageReactions 
  | DirectMessageTyping 
  | MessageContent 
  deriving (Eq, Show)
