module Haskcord.User where

import Universum
import qualified Discord.Types as DT
import Discord.Interactions
import Discord
import Discord.Requests
import GHC.Records (HasField (..))

-- data User = User
--   { id          :: DT.UserId
--   , name        :: Text
--   , discrim     :: Maybe Text
--   , avatar      :: Maybe Text
--   , isBot       :: Bool
--   , isWebhook   :: Bool
--   , isSystem    :: Maybe Bool
--   , mfa         :: Maybe Bool
--   , banner      :: Maybe Text
--   , accentColor :: Maybe Int
--   , locale      :: Maybe Text
--   , verified    :: Maybe Bool
--   , email       :: Maybe Text
--   , flags       :: Maybe Integer
--   , premiumType :: Maybe Integer
--   , publicFlags :: Maybe Integer
--   , member      :: Maybe DT.GuildMember
--   } deriving (Show, Read, Eq, Ord)

data VoiceChannel = VoiceChannel
  { id :: DT.ChannelId
  , voiceChannelName :: Text
  , guildId  :: DT.GuildId
  , users    :: [DT.User]
  , position :: Integer
  , nsfw     :: Bool
  }

-- fromDiscordHaskell :: DT.User -> User
-- fromDiscordHaskell DT.User{..} = User
--   { id          = userId
--   , name        = userName
--   , discrim     = userDiscrim
--   , avatar      = userAvatar
--   , isBot       = userIsBot
--   , isWebhook   = userIsWebhook
--   , isSystem    = userIsSystem
--   , mfa         = userMfa
--   , banner      = userBanner
--   , accentColor = userAccentColor
--   , locale      = userLocale
--   , verified    = userVerified
--   , email       = userEmail
--   , flags       = userFlags
--   , premiumType = userPremiumType
--   , publicFlags = userPublicFlags
--   , member      = userMember
--   }

instance HasField "id" DT.User DT.UserId where
  getField user = user.userId

instance HasField "name" DT.User Text where
  getField user = user.userName

instance HasField "name" VoiceChannel Text where
  getField vc = vc.voiceChannelName

instance HasField "name" DT.Channel Text where
  getField vc = vc.channelName

name record = record.name

moveTo :: VoiceChannel -> DT.User -> ReaderT Interaction DiscordHandler ()
moveTo vc user = do
  res <- lift $ restCall $ ModifyGuildMember vc.guildId user.id $ def { modifyGuildMemberOptsMoveToChannel = Just vc.id }
  print res

