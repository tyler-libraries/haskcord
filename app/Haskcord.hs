{-# LANGUAGE ScopedTypeVariables, TypeOperators #-}
module Haskcord
  ( module Named
  , module Haskcord
  , module Haskcord.User
  , module Haskcord.Types
  , module Haskcord.SlashCommands.Types
  , module Haskcord.SlashCommands.Callback
  , module Haskcord.SlashCommands.Constructors
  , ChannelId
  ) where

import Universum
import Named

import Discord
import Discord.Types
import Discord.Requests
import Discord.Interactions
import qualified Data.Text as T
import qualified Data.Text.Lazy as LT

import Haskcord.User
import Haskcord.Types
import qualified Haskcord.Intent as Intent
import Haskcord.SlashCommands.Types
import Haskcord.SlashCommands.Callback
import Haskcord.SlashCommands.Constructors

import Haskcord.SlashCommands.Internal.ConstructorUtils (display, emptyFn)

import PyF
import Text.Pretty.Simple

i = fmt

runHaskcord
  :: "token"                  :! Text
  -> "commands"               :? [Command]
  -> "intents"                :? [Intent.Gateway]
  -> "deleteOldCommands"      :? Bool
  -> "registerCommands"       :? Bool
  -> "onLog"                  :? (Text -> IO ())
  -> "onReady"                :? DiscordHandler ()
  -> IO Text
runHaskcord
  (Arg token)
  (argDef #commands               []      -> commands)
  (argDef #intents                []      -> intents)
  (argDef #deleteOldCommands      False   -> deleteOldCommandsFlag)
  (argDef #registerCommands       True    -> registerCommandsFlag)
  (argDef #onLog                  emptyFn -> onLog)
  (argDef #onReady              (pure ()) -> onReady)
  = runDiscord $ def
    { discordToken = token
    , discordOnLog = onLog . LT.toStrict . pShow . T.filter (`notElem` ['"', '\\'])
    , discordGatewayIntent
    , discordOnEvent = handle commands deleteOldCommandsFlag registerCommandsFlag onLog onReady
    , discordEnableCache = True
    }

  where
    f intent gatewayIntent = (intent `elem` intents) || gatewayIntent def 
    discordGatewayIntent = GatewayIntent
      { gatewayIntentGuilds                 = f Intent.Guilds gatewayIntentGuilds
      , gatewayIntentMembers                = f Intent.Members gatewayIntentMembers 
      , gatewayIntentEmojis                 = f Intent.Emojis gatewayIntentEmojis 
      , gatewayIntentBans                   = f Intent.Bans gatewayIntentBans 
      , gatewayIntentIntegrations           = f Intent.Integrations gatewayIntentIntegrations 
      , gatewayIntentWebhooks               = f Intent.Webhooks gatewayIntentWebhooks 
      , gatewayIntentInvites                = f Intent.Invites gatewayIntentInvites 
      , gatewayIntentVoiceStates            = f Intent.VoiceStates gatewayIntentVoiceStates 
      , gatewayIntentPresences              = f Intent.Presences gatewayIntentPresences 
      , gatewayIntentMessageChanges         = f Intent.MessageChanges gatewayIntentMessageChanges 
      , gatewayIntentMessageReactions       = f Intent.MessageReactions gatewayIntentMessageReactions 
      , gatewayIntentMessageTyping          = f Intent.MessageTyping gatewayIntentMessageTyping 
      , gatewayIntentDirectMessageChanges   = f Intent.DirectMessageChanges gatewayIntentDirectMessageChanges 
      , gatewayIntentDirectMessageReactions = f Intent.DirectMessageReactions gatewayIntentDirectMessageReactions 
      , gatewayIntentDirectMessageTyping    = f Intent.DirectMessageTyping gatewayIntentDirectMessageTyping 
      , gatewayIntentMessageContent         = f Intent.MessageContent gatewayIntentMessageContent 
      }


handle
  :: [Command]
  -> Bool
  -> Bool
  -> (Text -> IO ())
  -> DiscordHandler ()
  -> Event 
  -> DiscordHandler ()
handle commands deleteOldCommandsFlag registerCommandsFlag onLog onReady = \case
  Ready _ _ _ _ _ _ (PartialApplication appId _) -> do
    results <- if registerCommandsFlag 
      then forM commands $ \command -> do
        res <- case command of
          Command(Right c, [] , _) ->
            Right . pure <$> restCall (CreateGlobalApplicationCommand appId c)
          Command(Right c, gIds, _) ->
            Right <$> forM gIds (\gId -> 
              restCall (CreateGuildApplicationCommand appId gId c))
          Command(Left err, _, _) -> pure $ Left err
            
        case res of
          Left err   -> lift (onLog (show err))
          Right cmds -> forM_ cmds $ lift . onLog . show
        pure res
      else pure []

    let guildIds = join $ forM commands $ \(Command(_, gIds, _)) -> gIds
    when deleteOldCommandsFlag $ deleteOldCommands appId guildIds results
    onReady

  InteractionCreate interaction -> do
    lift $ onLog "interaction create started"
    lift $ onLog $ LT.toStrict $ pShow interaction
    forM_ commands $ \(Command(cmdOpt, _, topLevelFns)) -> case cmdOpt of
      Left err  -> lift $ onLog err
      Right cmd ->
        case interaction of
          InteractionApplicationCommand
            { applicationCommandData=ApplicationCommandDataChatInput
              { applicationCommandDataName=name
              , optionsData=opts
              }
            } | name == cmd.createName -> do
                lift $ onLog $ LT.toStrict $ pShow interaction
                lift . onLog $ display topLevelFns
                case (topLevelFns, opts) of               
                  (CommandCallbackLeaf n fn, Just (OptionsDataValues topOpts))
                    | n == name -> fn topOpts interaction
                  (CommandCallbackNode n sbcmdOrGroupFnss, Just (OptionsDataSubcommands sbcmds)) 
                    | n == name ->
                      forM_ sbcmdOrGroupFnss $ \sbcmdOrGroupFns ->
                        forM_ sbcmds $ \sbcmdOrGroup ->
                          case (sbcmdOrGroupFns, sbcmdOrGroup) of
                            (CommandCallbackNode sbcmdGroupName sbcmdFnss, OptionDataSubcommandGroup{..})
                              | sbcmdGroupName == optionDataSubcommandGroupName ->
                                forM_ sbcmdFnss $ \subcmdFns ->
                                  forM_ optionDataSubcommandGroupOptions $ \sbcmd ->
                                    case subcmdFns of
                                      CommandCallbackLeaf sbcmdName fn
                                        | sbcmdName == sbcmd.optionDataSubcommandName ->
                                          fn sbcmd.optionDataSubcommandOptions interaction
                                      _ -> lift $ onLog [fmt|{display subcmdFns} is not a Leaf of name {sbcmd.optionDataSubcommandName}|]
                            (CommandCallbackLeaf sbcmdName fn, OptionDataSubcommandOrGroupSubcommand sbcmd)
                              | sbcmdName == sbcmd.optionDataSubcommandName -> fn sbcmd.optionDataSubcommandOptions interaction
                            _ -> lift $ onLog [fmt|{display sbcmdOrGroupFns} is not a node or leaf aligning with ???|]
                  _ -> lift $ onLog "topLevelFn/opt combination not hit"
          _ -> lift $ onLog "interaction not handled"
  _ -> pure ()

  where
    deleteOldCommands :: ApplicationId -> [GuildId] -> [Either Text [Either RestCallErrorCode ApplicationCommand]] -> DiscordHandler ()
    deleteOldCommands appId guildIds results = unregisterGuildAndGlobalCmds (rights results >>= rights)
      where
      unregisterCmds validCmds delete = \case
        Left err -> lift . onLog $ "Failed to get registered slash commands: " <> show err

        Right cmds -> do
          let 
            validIds    = map applicationCommandId validCmds
            outdatedIds = filter (`notElem` validIds) . map applicationCommandId $ cmds
          forM_ outdatedIds $ restCall . delete 

      unregisterGuildAndGlobalCmds validCmds = do
        lift $ onLog $ "Deleting old slash commands, found: " <> show (length validCmds) <> " guild commands"
        forM_ guildIds $ \guildId ->
          restCall (GetGuildApplicationCommands appId guildId) 
            >>= unregisterCmds validCmds (DeleteGuildApplicationCommand appId guildId)
        restCall (GetGlobalApplicationCommands appId)  
          >>= unregisterCmds validCmds (DeleteGlobalApplicationCommand appId)

respond :: Text -> CommandIO ()
respond text = do
  ctx <- ask
  lift . void . restCall $ CreateInteractionResponse 
    ctx.interactionId 
    ctx.interactionToken 
    $ interactionResponseBasic text
