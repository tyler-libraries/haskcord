{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

import Universum
import Discord
import Discord.Types
import Discord.Interactions
import UnliftIO (liftIO)
import Data.Text (Text)
import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO)
import qualified Discord.Requests as R
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Text.Pretty.Simple
import qualified Data.Map as M

-- MAIN

main :: IO ()
main = do
  tok <- readFile "token"
  testGuildId <- case readMaybe "432305063730610176" of
    Just g -> pure g
    Nothing -> error "yeet"

  botTerminationError <- runDiscord $ def
    { discordToken = tok
    , discordOnEvent = onDiscordEvent testGuildId
    , discordOnLog = putTextLn
    -- If you are using application commands, you might not need
    -- message contents at all
    , discordGatewayIntent = def { gatewayIntentMembers = True }
--     , discordGatewayIntent = GatewayIntent t f f f f f f t f f f f f f f f
    }

  echo $ "A fatal error occurred: " <> botTerminationError
  where
    t = True
    f = False


-- UTILS

echo :: MonadIO m => Text -> m ()
echo = liftIO . TIO.putStrLn

showT :: Show a => a -> Text
showT = T.pack . show


-- COMMANDS

data SlashCommand = SlashCommand
  { name :: Text
  , registration :: Maybe CreateApplicationCommand
  , handler :: Interaction -> Maybe OptionsData -> DiscordHandler ()
  }

mySlashCommands :: [SlashCommand]
mySlashCommands = [ping]

ping :: SlashCommand
ping = SlashCommand
  { name = "ping"
  , registration = createChatInput "ping" "responds pong"
  , handler = \intr _options ->
      void . restCall $
        R.CreateInteractionResponse
          (interactionId intr)
          (interactionToken intr)
          (interactionResponseBasic  "pong")
  }


-- EVENTS

-- rickroll :: Channel -> DiscordHandler ()
-- rickroll c@(ChannelVoice {}) = do
--     result <- runVoice $ do
--         join (channelGuild c) (channelId c)
--         playYouTube "https://www.youtube.com/watch?v=dQw4w9WgXcQ"

--     case result of
--         Left err -> liftIO $ print err
--         Right _  -> pure ()

onDiscordEvent :: GuildId -> Event -> DiscordHandler ()
onDiscordEvent testGuildId event = do
  putTextLn $ "some event " <> T.take 40 (show event)
--   putTextLn (show event)
--   cache <- readCache
  -- let 
  --   guilds = M.elems $ cacheGuilds cache
  --   channels = join $ mapMaybe guildChannels guilds
  --   members = join $ mapMaybe guildMembers guilds
  -- print $ length members
  -- print $ map (\m -> memberUser m <&> userName) members
  -- print $ length channels
--  pPrint members

  case event of
    Ready _ _ _ _ _ _ (PartialApplication appId _) -> onReady appId testGuildId
    InteractionCreate intr                         -> onInteractionCreate intr
    ChannelUpdate c -> print c
    GuildCreate guild create -> putTextLn "guildCreate" >> pPrint create
      -- res <- sendCommand . RequestGuildMembers $ RequestGuildMembersOpts
      --   { requestGuildMembersOptsGuildId = guildId guild
      --   , requestGuildMembersOptsLimit = 100
      --   , requestGuildMembersOptsNamesStartingWith = ""
      --   }
      -- print res
    UnknownEvent txt obj -> putStrLn txt >> print obj
    _                                              -> pure ()

onReady :: ApplicationId -> GuildId -> DiscordHandler ()
onReady appId testGuildId = do
  echo "Bot ready!"

  appCmdRegistrations <- mapM tryRegistering mySlashCommands

  case sequence appCmdRegistrations of
    Left err ->
      echo $ "[!] Failed to register some commands" <> showT err

    Right cmds -> do
      echo $ "Registered " <> showT (length cmds) <> " command(s)."
      -- unregisterOutdatedCmds cmds

  where
  tryRegistering cmd = case registration cmd of
    Just reg -> restCall $ R.CreateGuildApplicationCommand appId testGuildId reg
    Nothing  -> pure . Left $ RestCallErrorCode 0 "" ""

  unregisterOutdatedCmds validCmds = do
    registered <- restCall $ R.GetGuildApplicationCommands appId testGuildId
    case registered of
      Left err ->
        echo $ "Failed to get registered slash commands: " <> showT err

      Right cmds ->
        let validIds    = map applicationCommandId validCmds
            outdatedIds = filter (`notElem` validIds)
                        . map applicationCommandId
                        $ cmds
         in forM_ outdatedIds $
              restCall . R.DeleteGuildApplicationCommand appId testGuildId

onInteractionCreate :: Interaction -> DiscordHandler ()
onInteractionCreate = \case
  cmd@InteractionApplicationCommand
    { applicationCommandData = input@ApplicationCommandDataChatInput {} } ->
      case
        find (\c -> applicationCommandDataName input == name c) mySlashCommands
      of
        Just found ->
          handler found cmd (optionsData input)

        Nothing ->
          echo "Somehow got unknown slash command (registrations out of date?)"

  _ -> pure ()
 
