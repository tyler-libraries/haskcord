import Universum
import Haskcord

main = do
  token   <- readFile "token"
  guildId <- readFile "guildId"

  res <- runHaskcord 
    ! #commands [greetingCommand guildId]
    ! #onLog putTextLn
    ! #token token 
    ! defaults
  print res
  
greetingCommand guildId = mkCommand 
  ! #name "greeting" 
  ! #description "a nice greeting"
  ! #guildIds [guildId]
  ! #options
    ( mkOption @Text ! #name "name" ! #description "your name" ! #required True' ! defaults
    , mkOption @VoiceChannel ! #name "age" ! #description "your age" ! #required True' ! defaults
    )
  ! #callback onGreeting
  ! defaults

onGreeting name age = respond [i|Greetings {name}|]

