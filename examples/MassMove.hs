import Universum
import Haskcord
import qualified Data.Text as T

main = do
  token   <- readFile "token"
  guildId <- readFile "guildId"

  void $ runHaskcord
    ! #commands [massMove guildId]
    ! #onLog putTextLn
    ! #token token
    ! defaults

massMove guildId = mkCommand
  ! #name "massmove"
  ! #description "moves users from one voice channel to another"
  ! #options (channelOpt "start", channelOpt "end")
  ! #callback onMassMove
  ! #guildIds [guildId]
  ! defaults
  where
    channelOpt name = mkOption @VoiceChannel
      ! #name [i|{name}_channel|]
      ! #description [i|{name}ing channel|]
      ! #required True'
      ! #autocomplete True
      ! defaults

onMassMove startChan endChan = do
  let names = T.intercalate "," $ map name startChan.users
  respond [i|Moving the following users: {names}|]
  forM_ startChan.users $ moveTo endChan
