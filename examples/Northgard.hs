{-# LANGUAGE ScopedTypeVariables #-}
import Universum
import Haskcord
import qualified Data.Text as T

main = do
  token  <- readFile "token"
  result <- runHaskcord
    ! #commands [northgardCommand]
    ! #deleteOldCommands True
    ! #onLog (\s -> putTextLn $ "ONYEET " <> s)
    ! #token token
    ! defaults
  print result

northgardCommand = mkCommandGroup 
  ! #name "northgard"
  ! #description "northgard commands"
  ! #guildIds ["432305063730610176"]
  ! #subcommandGroups [warbandSubcommandGroup, tradeSubcommandGroup]
  ! defaults

warbandSubcommandGroup = mkSubcommandGroup 
  ! #name "warband"
  ! #description "warband subcommands"
  ! #subcommands [cost, advancedCost]
  ! defaults
  where
    cost = mkSubcommand 
      ! #name "cost" 
      ! #description "calculate warband cost"
      ! #callback onCost
      ! #options (countOpt "", unitTypeOpt, warEffortOpt)
      ! defaults
    advancedCost = mkSubcommand
      ! #name "cost_advanced" 
      ! #description "Calculate warband cost with more information"
      ! #callback onAdvancedCost
      ! #options 
        ( countOpt "warrior"
        , countOpt "axe_thrower"
        , countOpt "shield_bearer"
        , countOpt "skirmisher"
        , warEffortOpt
        )
      ! defaults

    countOpt kind = mkOption @Integer 
      ! #name [i|{kind'}count|]
      ! #description ([i|{desc}|] :: Text)
      ! #required True'
      ! defaults
      where 
        kind' = if null kind then "" else kind <> "_"
        desc :: String
        desc = map (\s -> if s == '_' then ' ' else s) [i|number of {kind'}units|]
    unitTypeOpt = mkOption @Text 
      ! #name "type" 
      ! #description "type of unit" 
      ! #required True'
      ! #choices ["Warrior", "Axe Thrower", "Shield Bearer", "Skirmisher", "Dragonkin", "Tracker", "Shaman"]
      ! defaults
    warEffortOpt = mkOption @Bool
      ! #name "war_effort" 
      ! #description "legions 500 military experience" 
      ! #required False'
      ! defaults

tradeSubcommandGroup = mkSubcommandGroup
  ! #name "trade"
  ! #description "trade subcommands"
  ! #subcommands [cost]
  ! defaults
  where
    cost = mkSubcommand
      ! #name "cost"
      ! #description "calculate trade route cost"
      ! #callback onTradeCost
      ! #options 
        ( mkOption @Integer 
          ! #name "gold_income" 
          ! #description "gold +- number, enter 0 if you want to see total trade route cost"
          ! #required True'
          ! defaults
        , mkOption @Text
          ! #name "route"
          ! #description "Route option. CI = Commercial Influence"
          ! #required True'
          ! #choices ["+5 CI; -5 gold", "+10 CI; -20 gold", "+15 CI; -40 gold", "+20 CI; -60 gold"] 
          ! defaults
        , mkOption @Integer
          ! #name "stag_fame"
          ! #description "enter a positive number if you're stag and have Glory of the Clan"
          ! #required False'
          ! defaults
        , mkOption @Bool
          ! #name "is_raven"
          ! #description "are you raven clan?"
          ! #required False'
          ! defaults
        )
      ! defaults

onCost count unitType warEffortOpt ctx = do
  let
    cost = calculateCost unitType count warEffortOpt
    typeStr = if count == 1 then unitType else unitType <> "s"
    warEffortStr = if fromMaybe False warEffortOpt then "with war effort " else ""
  respond ctx [i|Total warband cost {warEffortStr}for {count} {typeStr}: {cost}|]

data Route = RouteFirst | RouteSecond | RouteThird | RouteFourth

onTradeCost :: Integer -> Text -> Maybe Integer -> Maybe Bool -> CommandContext -> HaskcordIO ()
onTradeCost positiveGoldPerTick routeText stagFameOpt isRavenOpt ctx = do
  let
    route = case toString routeText of
      '+':'5':_     -> RouteFirst
      '+':'1':'0':_ -> RouteSecond
      '+':'1':'5':_ -> RouteThird
      '+':'2':'0':_ -> RouteFourth

    negativeGoldPerTick :: Double = case route of
      RouteFirst  -> 5.0
      RouteSecond -> 20.0
      RouteThird  -> 40.0
      RouteFourth -> 60.0

    ciMultiplier = case stagFameOpt of
      Just stagFame -> 1 + (fromInteger stagFame / 2000.0)
      Nothing       -> 1

    ciPerTick = ciMultiplier * case route of
      RouteFirst  -> 5
      RouteSecond -> 10
      RouteThird  -> 15
      RouteFourth -> 20

    netGoldPerMonth = (fromInteger positiveGoldPerTick - negativeGoldPerTick) * 6
    monthsNeeded = 2000 / (ciPerTick * 6)
    totalGoldNeededOrExcess = netGoldPerMonth * monthsNeeded

  respond ctx $ [i|You'll win in: {monthsNeeded} months\n|] <> case totalGoldNeededOrExcess of
    0 -> "You'll have exactly 0 gold when finished"
    n | n < 0 -> [i|You need {abs n} gold to win at those rates|]
    n         -> [i|You'll have {n} gold leftover once you win|]

onAdvancedCost warriorCount axeThrowerCount shieldBearerCount skirmisherCount warEffortOpt ctx = do
  let
    waCost = calculateCost "Warrior" warriorCount warEffortOpt
    atCost = calculateCost "Axe Thrower" axeThrowerCount warEffortOpt
    sbCost = calculateCost "Shield Bearer" skirmisherCount warEffortOpt
    skCost = calculateCost "Skirmisher" skirmisherCount warEffortOpt

    unindent = unlines . map (T.dropWhile (== ' ')) . lines

  respond ctx $ unindent [i|```asciidoc
    [Warrior]
    Count :: {warriorCount}
    Cost  :: {waCost}

    [Axe Throwers]
    Count :: {axeThrowerCount}
    Cost  :: {atCost}

    [Shield Bearers]
    Count :: {shieldBearerCount}
    Cost  :: {sbCost}

    [Skirmishers]
    Count :: {skirmisherCount}
    Cost  :: {skCost}

    = TOTAL = -- this isn't currently accurate
    Cost :: {waCost + atCost + sbCost + skCost}
  ```
  |]

calculateCost unitType count warEffortOpt = cost
  where
    warEffort = fromMaybe False warEffortOpt
    (baseCost, increment) = case unitType of
      u | u `elem` ["Warrior", "Shield Bearer", "Axe Thrower"] -> (30, 6)
      "Skirmisher" -> (20, 5)
      "Dragonkin"  -> (40, 10)
      "Tracker"    -> (25, 5)
      "Shaman"     -> (30, 5)
      _            -> error "Case not handled"

    startingCost = if warEffort then floor (baseCost * 0.75) else baseCost
    cost = iterate (+ increment) startingCost & take (fromInteger count) & sum


