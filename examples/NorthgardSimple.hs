import Universum
import Haskcord

main = do
  token  <- readFile "token"
  result <- runHaskcord
    ! #commands [northgardCommand]
    ! #deleteOldCommands True
    ! #token token
    ! defaults
  print result

northgardCommand = mkCommandGroup 
  ! #name "northgard"
  ! #description "northgard commands"
  ! #subcommandGroups [warband]
  ! defaults
  where
    warband = mkSubcommandGroup 
      ! #name "warband"
      ! #description "warband subcommands"
      ! #subcommands [cost]
      ! defaults

    cost = mkSubcommand 
      ! #name "cost" 
      ! #description "calculate warband cost"
      ! #callback onCost
      ! #options (countOpt, unitTypeOpt, warEffortOpt)
      ! defaults

    countOpt = mkOption @Integer 
      ! #name "count"
      ! #description "number of units"
      ! #required True'
      ! defaults
    unitTypeOpt = mkOption @Text 
      ! #name "type" 
      ! #description "type of unit" 
      ! #required True'
      ! #choices 
        [ "Warrior", "Axe Thrower", "Shield Bearer", "Skirmisher"
        , "Dragonkin", "Tracker", "Shaman"
        ]
      ! defaults
    warEffortOpt = mkOption @Bool
      ! #name "war_effort" 
      ! #description "legions 500 military experience" 
      ! #required False'
      ! defaults


onCost count unitType warEffortOpt ctx = do
  let
    warEffort = fromMaybe False warEffortOpt
    (baseCost, increment) = case unitType of
      u | u `elem` ["Warrior", "Shield Bearer", "Axe Thrower"] -> (30, 6)
      "Skirmisher" -> (20, 5)
      "Dragonkin"  -> (40, 10)
      "Tracker"    -> (25, 5)
      "Shaman"     -> (30, 5)
      _            -> error "Case not handled"
    startingCost = if warEffort then floor (baseCost * 0.75) else baseCost
    cost = iterate (+ increment) startingCost & take (fromInteger count) & sum
    typeStr = if count == 1 then unitType else unitType <> "s"
    warEffortStr = if warEffort then "with war effort " else ""
  respond ctx [i|Total warband cost {warEffortStr}for {count} {typeStr}: {cost}|]

