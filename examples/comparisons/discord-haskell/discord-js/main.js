// Import discord.js module
const { Client, REST, Routes, GatewayIntentBits, ApplicationCommandOptionType } = require('discord.js');

const TOKEN = ""
const CLIENT_ID = ""
const GUILD_ID = ""
const rest = new REST({ version: '10' }).setToken(TOKEN);

commands = [{
  name: 'move',
  description: 'Moves all users from one channel to another',
  options: [
    {
      name: 'source',
      type: ApplicationCommandOptionType.Channel,
      description: 'The voice channel to move users from',
      required: true
    },
    {
      name: 'destination',
      type: ApplicationCommandOptionType.Channel,
      description: 'The voice channel to move users to',
      required: true
    }
  ]
}];

(async () => {
  try {
    console.log('Started refreshing application (/) commands.');

    await rest.put(Routes.applicationGuildCommands(CLIENT_ID, GUILD_ID), { body: commands });

    console.log('Successfully reloaded application (/) commands.');
  } catch (error) {
    console.error(error);
  }
})();

const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildVoiceStates] });

client.once('ready', () => {
  console.log('Ready!');
});


// Listen for the interactionCreate event
client.on('interactionCreate', async interaction => {
  // Check if the interaction is a command with the name move
  if (!interaction.isCommand()) return;
  if (interaction.commandName === 'move') {
    // Get the source and destination channels from the command options
    console.log(interaction.options);
    const source = interaction.options.getChannel('source');
    const destination = interaction.options.getChannel('destination');
    console.log(source);
    console.log("AND");
    console.log(destination);
    console.log(!source);
    console.log(!destination);
    // Check if both channels are valid voice channels and if they are different
    if (!source || !destination) {
      return interaction.reply('Please provide valid voice channels.');
    }
    if (source.type !== 2 || destination.type !== 2) {
      return interaction.reply('Please provide voice channels only.');
    }
    if (source.id === destination.id) {
      return interaction.reply('Please provide different voice channels.');
    }
    // Loop through all the members in the source channel and move them to the destination channel

    try {
      const members = source.members.values();
      for (const member of members) {
        console.log(member);
        await member.voice.setChannel(destination);
      }
      // Reply with a success message (ephemeral)
      interaction.reply({ content: `Moved ${members.length} users from ${source.name} to ${destination.name}.`, ephemeral: true });
    } catch (error) {
      // Catch any errors and reply with an error message
      console.error(error);
      interaction.reply({ content: 'Something went wrong while moving users.', ephemeral: true });
    }
  }
});


client.login(TOKEN);
