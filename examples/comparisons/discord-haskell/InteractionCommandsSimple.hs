import Universum
import Haskcord

main = do
  token   <- readFile "token"
  guildId <- readFile "guildId"

  botError <- runHaskcord
    ! #commands [ping guildId]
    ! #deleteOldCommands True
    ! #token token
    ! defaults

  putTextLn $ "A fatal error occurred: " <> botError

ping guildId = mkCommand
  ! #name "ping"
  ! #description "responds pong"
  ! #options ()
  ! #callback (respond "pong")
  ! #guildIds [guildId]
  ! defaults

