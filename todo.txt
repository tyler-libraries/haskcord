(C) 2023-03-03 Investigate removing (Maybe args) from HaskcordOptions, only needed to disambiguate types at callsite, might be able to use type applications or some other method to help the compiler along
(B) 2023-03-04 `SendMessage` typeclass with `sendMessage` fn that sends messages based on various contexts or conditions, as `respond` will only be for generating initial response to interactions or similar things @Feature
(B) 2023-03-04 Move large chunks of handle to Haskcord.SlashCommands as handle will have more than just slash commands @Refactor
(A) 2023-03-14 add initial voice states, voice channel opt, and ability to move users between channels @Feature
